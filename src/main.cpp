/*This code is a microcontroller implementation for an IoT data source
It runs on an arduino mega 2560 board, and uses an ENC28J60 SPI ethernet controller.

Connections:
Eth -> Arduino:
------------
SO  -> 50
SCK -> 52
RST -> RESET
GND -> GND
SI  -> 51
CS  -> 53
VCC -> 3.3V

Daniel McCarthy - 05/2018
*/

#include <Arduino.h>
#include <UIPEthernet.h>

#define LISTENPORT 2560
//#define debug  //Comment this out to compile with no debug output (Runs faster)
#define UARTBAUD 9600

EthernetServer tcp = EthernetServer(LISTENPORT);

void setup() {
    //Initialise Network
    uint8_t mac[6] = {0xDE,0x01,0x02,0x03,0x04,0x05};
    Ethernet.begin(mac); //Configure IP address via DHCP
    delay(1000);
    tcp.begin();         //Bind TCP Listener to port 2560

    #ifdef debug
    Serial.begin(UARTBAUD); //UART for debug
    #endif
}

void loop() {
    //Recieve TCP Connections and deal with them
    size_t size;
    if(EthernetClient client = tcp.available()) {
        while ((size = client.available()) > 0) {
            uint8_t* msg = (uint8_t*)malloc(size+1); //Get memory for message
            memset(msg, 0, size+1); //Zero out memory

            size = client.read(msg, size); //Read buffer

            //Parse Request
            switch((char)msg[0]) {
                case 'A': //Analog Read
                    if(msg[1] <= 15) { //Check if sane
                        int adcvalue = analogRead(msg[1]);
                        //Get high and low bytes
                        char bytes[2];
                        bytes[0] = adcvalue;
                        bytes[1] = adcvalue >> 8;
                        client.write(bytes, sizeof(bytes)); //Send ADC value
                    }
                    break;

                case 'D': //Digital Read
                    if(msg[1] < 50) { //Check if sane (No touching the SPI pins 50-53)
                        uint8_t response = digitalRead(msg[1]) ? 0xFF : 0x00;
                        client.write(response); //Read/Send Digital value
                    }
                    break;
                default:
                    client.stop(); //We got an unsupported request
            }
            #ifdef debug
            Serial.println((char*)msg);
            #endif
            free(msg); //Free memory
        }
    }
}