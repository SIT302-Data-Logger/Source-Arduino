#! /usr/bin/python3
import socket, sys, time

port = 2560
n = 1000

if(len(sys.argv) > 1):
    addr = sys.argv[1]
else:
    addr = "10.0.2.21"

sock = socket.create_connection((addr, port))

startT = (time.time())

for i in range(0, n):
    start = time.time() * 1000
    msg = b"A" + bytes([0])
    sock.send(msg)
    reply = sock.recv(2) #Recv 2 bytes
    print(int.from_bytes(reply, byteorder='little'))
    msg = b"A" + bytes([1])
    sock.send(msg)
    reply = sock.recv(2) #Recv 2 bytes
    print(int.from_bytes(reply, byteorder='little'))
    while ((time.time() * 1000) < start + 100):
        pass



totaltime = time.time() - startT

print (totaltime)
print (totaltime/n)


sock.close()


